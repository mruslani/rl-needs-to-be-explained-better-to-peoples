# RL needs to be explained better to peoples

I mean, seriously. There are many good blogs out there, but every one of them seems to be missing some part of explanations of key definitions, which doesn't let to just grasp RL all at once. I have to keep 3-4 of these blog tabs open to collect the pieces and build the whole picture of what RL actually does. 

I'll dare to write something more comprehensive, if I won't be too lazy. The page that I will put together, will be a superposition of the following blogs:
- [OpenAI](https://spinningup.openai.com/en/latest/spinningup/spinningup.html)
- [Dhanoop Karunakaran blog](https://medium.com/intro-to-artificial-intelligence/the-actor-critic-reinforcement-learning-algorithm-c8095a655c14)
- [Lil'Log](https://lilianweng.github.io/posts/2018-02-19-rl-overview/)
- [Blog of some guy with name Chris](https://cgnicholls.github.io/reinforcement-learning/2017/03/27/a3c.html)
